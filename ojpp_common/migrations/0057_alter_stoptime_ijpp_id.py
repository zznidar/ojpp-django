# Generated by Django 4.1.5 on 2023-02-01 14:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("ojpp_common", "0056_timetableperiod_remove_vehicle_photo_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="stoptime",
            name="ijpp_id",
            field=models.PositiveIntegerField(blank=True, null=True, unique=True),
        ),
    ]
