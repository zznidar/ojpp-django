# Generated by Django 4.1.3 on 2022-11-20 16:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("ojpp_common", "0020_remove_vehiclelocation_lat_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="vehicle",
            name="model",
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
