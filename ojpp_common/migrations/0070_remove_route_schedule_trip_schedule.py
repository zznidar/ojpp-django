# Generated by Django 4.1.7 on 2023-03-20 15:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ojpp_common', '0069_stoplocation_operator_stop_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='route',
            name='schedule',
        ),
        migrations.AddField(
            model_name='trip',
            name='schedule',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ojpp_common.schedule'),
        ),
    ]
