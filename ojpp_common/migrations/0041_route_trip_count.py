# Generated by Django 4.1.4 on 2022-12-14 10:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("ojpp_common", "0040_alter_operator_options_alter_route_options_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="route",
            name="trip_count",
            field=models.PositiveSmallIntegerField(default=0, editable=False),
        ),
    ]
