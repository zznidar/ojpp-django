# Generated by Django 4.1.6 on 2023-02-10 13:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ojpp_common', '0061_timetableexception_ijpp_id_timetableperiod_ijpp_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='operator',
            name='vehicle_count',
        ),
        migrations.RemoveField(
            model_name='route',
            name='geometry',
        ),
        migrations.RemoveField(
            model_name='route',
            name='trip_count',
        ),
    ]
