#!/usr/bin/env python
import django
import os

from pathlib import Path
from transitfeed import Schedule, Route as GTFSRoute, Trip as GTFSTrip, Shape
from django.db.models import Q

##############
#   DJANGO   #
##############
if __name__ == '__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ojpp_core.settings")
    django.setup()

from ojpp_common.models import *


##############
#   CONFIG   #
##############
OUTPUT = Path('ijpp_gtfs.zip')


def main():
    #######################
    #   INITIALIZE GTFS   #
    #######################

    schedule = Schedule()

    agencies_by_id = {}
    gstops = {}

    stops = StopLocation.objects.exclude(ijpp_id__isnull=True)
    for stop in stops:
        gstops[stop.id] = schedule.AddStop(lat=stop.location.coords[1], lng=stop.location.coords[0], name=stop.stop.name, stop_id=str(stop.id))

    routes = Route.objects.prefetch_related(
        'first_stop',
        'last_stop',
        'trips',
        'trips__stoptimes',
        'trips__stoptimes__stop_location__stop'
    ).exclude(
        Q(ijpp_id__isnull=True) |
        Q(first_stop__isnull=True, last_stop__isnull=True) |
        Q(trips__stoptimes__time_departure__exact="00:00:00")
    )

    #########################################
    #   INSERT ROUTES, TRIPS & STOP TIMES   #
    #########################################

    for route in routes:

        if not route.operator_id in agencies_by_id:
            agencies_by_id[route.operator_id] = schedule.AddAgency(
                route.operator.name, route.operator.url, route.operator.timezone, agency_id=route.operator.id
            )

        sroute: GTFSRoute = schedule.AddRoute(
            short_name="",
            long_name=route.name or f"{route.first_stop.name}-{route.last_stop.name}",
            route_type="Bus",
            route_id=route.id
        )
        for trip in route.trips.all():
            trip: Trip

            rtrip: GTFSTrip = sroute.AddTrip(schedule, trip_id=trip.id)
            for stoptime in trip.stoptimes.order_by('sequence_num'):
                if stoptime.time_departure is not None:
                    rtrip.AddStopTime(
                        gstops[stoptime.stop_location_id],
                        stop_time=stoptime.time_departure.strftime("%H:%M:%S")
                    )


    #####################
    #   GENERATE GTFS   #
    #####################

    print("Validating:")
    print()

    schedule.Validate()

    print()
    print("Writing file...")

    schedule.WriteGoogleTransitFeed(OUTPUT)

    print("Done!")


if __name__ == '__main__':
    main()