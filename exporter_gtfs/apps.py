from django.apps import AppConfig


class ExporterIJPPConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "exporter_gtfs"
