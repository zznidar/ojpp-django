from django.apps import AppConfig


class AdapterArrivaConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "adapter_arriva"
