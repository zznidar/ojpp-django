from django.apps import AppConfig


class AdapterApmsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "adapter_apms"
